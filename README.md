# ManaCMF
This is a content management framework(ManaCMF) based on ManaPHP Framework.

##Installation

The preferred way to install the ManaCMF Application is through [composer](http://getcomposer.org/download/).

```
composer create-project xlstudio/manacmf:dev-master
```

<p align="center">
<a href="https://travis-ci.org/xlstudio/manacmf"><img src="https://travis-ci.org/xlstudio/manacmf.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/xlstudio/manacmf"><img src="https://poser.pugx.org/xlstudio/manacmf/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/xlstudio/manacmf"><img src="https://poser.pugx.org/xlstudio/manacmf/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/xlstudio/manacmf"><img src="https://poser.pugx.org/xlstudio/manacmf/license.svg" alt="License"></a>
</p>
