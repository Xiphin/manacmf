<?php

namespace Application\Home\Controllers;

use ManaPHP\Mvc\Controller;

class IndexController extends ControllerBase
{
    public function indexAction()
    {
        $respStr = "This is ok! The web application is running!";

        return response()->setcontent($respStr);
    }
}